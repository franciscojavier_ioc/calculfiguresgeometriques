package ioc.xtec.cat.calculfiguresgeometriques;

import java.util.Scanner;

public class Cercle implements FiguraGeometrica{
    private final double costat;
    
    public Cercle() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduïu la longitud del costat del cercle: ");
        this.costat = scanner.nextDouble();
    }
    
    @Override
    public double calcularArea() {
        return costat * costat;
    }
    
    @Override
    public double calcularPerimetre() {
        return 4 * costat;
    }
}
